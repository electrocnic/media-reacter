import React from 'react';
import ResizableDivContainer from '@/components/ResizeableDivContainer';
import useTooltipInjector from '@/utils/useTooltipInjector';

const App: React.FC = () => {
    useTooltipInjector()

    return (
        <div id='app' style={{ display: 'flex', flexWrap: 'wrap' }}>
            <ResizableDivContainer />
        </div>
    );
};

export default App;
