const ToolTipTexts = {
    freeAspectRatio: "Hold left ALT to free aspect ratio",
    freeSnap: "Hold CTRL to ignore snap",
    freeBoth: "CTRL: ignore snap, LEFT ALT: ignore aspect ratio",
    zIndexScroll: "Scroll for Z level",
};

export default ToolTipTexts;
