import React, { useState, useEffect, useRef } from 'react';

const MediaInDiv: React.FC = () => {
    const [media, setMedia] = useState<File | null>(null);
    const [mediaSrc, setMediaSrc] = useState<string | null>(null);
    const [mediaType, setMediaType] = useState<'image' | 'video' | null>(null);
    const [mediaDimensions, setMediaDimensions] = useState({ width: 300, height: 300 });
    const fileInputRef = useRef<HTMLInputElement>(null);

    useEffect(() => {
        // Generate and set media source URL when media state updates
        if (media) {
            const src = URL.createObjectURL(media);
            setMediaSrc(src);

            // Cleanup function to revoke object URL to avoid memory leaks
            return () => {
                URL.revokeObjectURL(src);
            };
        }
    }, [media]);

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const files = event.target.files;
        if (files && files[0]) {
            const file = files[0];
            setMedia(file);

            if (file.type.startsWith('image')) {
                setMediaType('image');
                // For images, if you need to read dimensions, use a similar approach with an Image object
            } else if (file.type.startsWith('video')) {
                setMediaType('video');
                const video = document.createElement('video');
                video.preload = 'metadata';
                video.src = URL.createObjectURL(file);
                video.onloadedmetadata = () => {
                    // Ensure the video dimensions are set once metadata is loaded
                    setMediaDimensions({width: video.videoWidth, height: video.videoHeight});
                    URL.revokeObjectURL(video.src); // Clean up the object URL
                };
            } else {
                // Handle unsupported file types if necessary
                console.error('Unsupported file type');
                return;
            }
        }
    };

    const loadMediaDimensions = () => {
        if (mediaSrc && mediaType === 'image') {
            const img = new Image();
            img.onload = () => {
                setMediaDimensions({ width: img.width, height: img.height });
            };
            img.src = mediaSrc;
        } // else if (mediaSrc && mediaType === 'video') {
        //     const video = new Video();
        //     video.onload = () => {
        //         setMediaDimensions({ width: video.width, height: video.height });
        //     }
        //     video.src = mediaSrc;
        // }
    };

    useEffect(() => {
        loadMediaDimensions();
    }, [mediaSrc]);


    /* TODO:
        * Resize with snap of images inside div.
        * Remove load button and replace by simple keyboard combo when mouse is over it
        * Add new media div with keyboard shortcut
        * Remove div with delete key when mouse over
        * Auto size div to image and auto lock div size to image size and vice versa: resizing div will resize image
        * Allow for unlock and resize/crop independently by key hold (maybe shift)
        * Allow for directory instead of individual image
        * Save media data with div positions and stuff to json
        * Allow videos and allow video time markers, replay settings.
    */

    return (
        <>
            <input
                type="file"
                ref={fileInputRef}
                onChange={handleFileChange}
                style={{ display: 'none' }}
                accept="image/*,video/*"
            />
            <button onClick={() => fileInputRef.current?.click()}>Load Media</button>
            <div
                style={{
                    width: `${mediaDimensions.width}px`,
                    height: `${mediaDimensions.height}px`,
                    overflow: 'hidden',
                    position: 'relative',
                }}
            >
                {mediaSrc && mediaType === 'image' && <img src={mediaSrc} alt="Loaded Media" style={{ width: '100%', height: 'auto' }} />}
                {mediaSrc && mediaType === 'video' && (
                    <video width="100%" height="auto" controls>
                        <source src={mediaSrc} type="video/mp4" /> {/* Consider dynamically setting the type based on the file */}
                        Your browser does not support the video tag.
                    </video>
                )}
            </div>
        </>
    );
};

export default MediaInDiv;
