import React, {useState, useEffect, useCallback, useRef} from 'react';
import type {ResizableDivProps, ResizeHandlePosition} from '@/types/types';
import {EdgePosition, CornerPosition, isInEnum} from '@/types/types';

import ToolTipTexts from "@/assets/ToolTipText.ts";
import MediaInDiv from "@/components/MediaInDiv.tsx";

const ResizableDiv: React.FC<ResizableDivProps> = (props) => {
    // Ref to the div element
    const divRef = useRef<HTMLDivElement>(null);

    const [aspectRatio, setAspectRatio] = useState(props.width.value / props.height.value);
    const [isDragging, setIsDragging] = useState(false);
    const [startPosition, setStartPosition] = useState({x: 0, y: 0});
    const [resizeDirection, setResizeDirection] = useState<string | null>(null);
    const [isMoving, setIsMoving] = useState(false);

    // Updated handleDragStart to differentiate between resize and move
    const handleDragStart = useCallback((direction: string) => (e: React.MouseEvent<HTMLDivElement>) => {
        e.preventDefault(); // Prevent unwanted text selection
        setIsDragging(true);
        setStartPosition({x: e.clientX, y: e.clientY});
        setResizeDirection(direction);
        setIsMoving(direction === 'move'); // Set moving flag based on direction
    }, []);

    const handleMouseDown = useCallback((e: React.MouseEvent<HTMLDivElement>) => {
        // Initiate dragging for moving
        handleDragStart('move')(e);
    }, [handleDragStart]);

    function resize(e: MouseEvent, deltaX: number, deltaY: number): {
        newX: number,
        newY: number,
        newWidth: number,
        newHeight: number,
        leftRight: 0 | 1 | -1,
        bottomTop: 0 | 1 | -1
    } {
        let newWidth = props.width.value;
        let newHeight = props.height.value;
        let newX = props.x.value;
        let newY = props.y.value;

        if (!resizeDirection) return {newX, newY, newWidth, newHeight, leftRight: 0, bottomTop: 0};

        const leftRight = resizeDirection.includes(EdgePosition.Right) ? 1 : (resizeDirection.includes(EdgePosition.Left) ? -1 : 0);
        const bottomTop = resizeDirection.includes(EdgePosition.Bottom) ? 1 : (resizeDirection.includes(EdgePosition.Top) ? -1 : 0);

        if (isInEnum(resizeDirection, CornerPosition)) {
            // Initial resizing logic
            let aspectDeltaX = deltaX;
            let aspectDeltaY = deltaY;

            // Adjust dimensions based on the corner
            if (resizeDirection === CornerPosition.TopLeft || resizeDirection === CornerPosition.BottomLeft) {
                newWidth -= deltaX; // Decrease width if pulling left
                aspectDeltaX = -deltaX; // Reverse the deltaX for aspect ratio calculation
            } else {
                newWidth += deltaX; // Increase width if pulling right
            }

            if (resizeDirection === CornerPosition.TopLeft || resizeDirection === CornerPosition.TopRight) {
                newHeight -= deltaY; // Decrease height if pulling up
                aspectDeltaY = -deltaY; // Reverse the deltaY for aspect ratio calculation
            } else {
                newHeight += deltaY; // Increase height if pulling down
            }

            if (!e.altKey) {
                // Maintain updated aspect ratio
                if (Math.abs(aspectDeltaX) > Math.abs(aspectDeltaY)) {
                    newHeight = newWidth / aspectRatio;
                } else {
                    newWidth = newHeight * aspectRatio;
                }
            } else {
                setAspectRatio(newWidth / newHeight); // update aspect ratio in flight to avoid jumps on leaving alt key.
            }

            // Adjust position for top and left resizes
            if (resizeDirection === CornerPosition.TopLeft || resizeDirection === CornerPosition.TopRight) {
                newY += props.height.value - newHeight; // Move up if decreasing height
            }

            if (resizeDirection === CornerPosition.TopLeft || resizeDirection === CornerPosition.BottomLeft) {
                newX += props.width.value - newWidth; // Move left if decreasing width
            }
        } else {
            newWidth += deltaX * leftRight;
            newHeight += deltaY * bottomTop;
            // Move x and y position if resizing from left or top
            if (leftRight === -1) { // Resizing from the left
                newX = props.x.value + deltaX;
            }
            if (bottomTop === -1) { // Resizing from the top
                newY = props.y.value + deltaY;
            }
        }

        return {newX, newY, newWidth: Math.max(newWidth, 20), newHeight: Math.max(newHeight, 20), leftRight, bottomTop};
    }

    const handleDrag = useCallback(
        (e: MouseEvent) => {
            if (!isDragging || !resizeDirection) return;

            if (props.x.value === Infinity) {
                props.x.value = 50;
            }
            if (props.y.value === Infinity) {
                props.y.value = 50;
            }

            const deltaX = e.clientX - startPosition.x;
            const deltaY = e.clientY - startPosition.y;

            if (isMoving) {
                // Invoke onDrag from parent to update position
                props.onDrag(e, props.x.value + deltaX, props.y.value + deltaY, props.id, props.horizontalEdges, props.verticalEdges);
            } else {
                const {newX, newY, newWidth, newHeight, leftRight, bottomTop} = resize(e, deltaX, deltaY);
                props.onResize(e, newX, newY, newWidth, newHeight, leftRight, bottomTop, props.id, props.horizontalEdges, props.verticalEdges);
            }

            setStartPosition({
                x: e.clientX,
                y: e.clientY,
            });
        },
        [isDragging, isMoving, startPosition, props.width.value, props.height.value, aspectRatio, resizeDirection, props.x.value, props.y.value, props.onDrag, props.id]
    );

    const handleDragEnd = useCallback(() => {
        setIsDragging(false);
        setIsMoving(false);
        // Update aspect ratio based on the final size to maintain it for future resizes
        setAspectRatio(props.width.value / props.height.value);
        setResizeDirection(null);

        props.horizontalSnaps.value = [];
        props.verticalSnaps.value = [];
    }, [props.width, props.height]);

    const handleWheel = useCallback((e: WheelEvent) => {
        e.preventDefault();
        // Determine the direction of scrolling
        const direction = e.deltaY < 0 ? 'up' : 'down';
        // Call a prop function passed from the container to adjust zIndex
        props.onAdjustZIndex(props.id!, direction);
    }, [props.id, props.onAdjustZIndex]); // onAdjustZIndex is a new prop function for adjusting zIndex

    useEffect(() => {
        const divElement = divRef.current;
        if (divElement) {
            divElement.addEventListener('wheel', handleWheel, {passive: false});
        }

        if (isDragging) {
            window.addEventListener('mousemove', handleDrag);
            window.addEventListener('mouseup', handleDragEnd);
        }

        return () => {
            window.removeEventListener('mousemove', handleDrag);
            window.removeEventListener('mouseup', handleDragEnd);
            if (divElement) {
                divElement.removeEventListener('wheel', handleWheel);
            }
        };
    }, [isDragging, handleDrag, handleDragEnd]);

    function handleResize(position: CornerPosition | EdgePosition) {
        return (e: React.MouseEvent<HTMLDivElement>) => {
            e.stopPropagation();
            // Assuming handleDragStart is a function that takes a CornerPosition and returns an event handler
            handleDragStart(position)(e);
        };
    }

    return (
        <div
            ref={divRef}
            style={{
                width: `${props.width.value}px`,
                height: `${props.height.value}px`,
                resize: 'none',
                overflow: 'hidden',
                backgroundColor: props.backgroundColor,
                position: 'absolute', // Use absolute positioning
                left: `${props.x.value}px`, // Position based on x
                top: `${props.y.value}px`, // Position based on y
                zIndex: props.zIndex.value
            }}
            onMouseDown={handleMouseDown} // Attach mouse down handler for moving
            tooltip={ToolTipTexts.zIndexScroll}
        >
            <MediaInDiv/>
            {/* Resize handles */}

            <div onMouseDown={handleResize(CornerPosition.TopLeft)}
                 style={resizeHandleStyles(CornerPosition.TopLeft)}
                 tooltip={ToolTipTexts.freeBoth}
            ></div>
            <div onMouseDown={handleResize(CornerPosition.TopRight)}
                 style={resizeHandleStyles(CornerPosition.TopRight)}
                 tooltip={ToolTipTexts.freeBoth}
            ></div>
            <div onMouseDown={handleResize(CornerPosition.BottomLeft)}
                 style={resizeHandleStyles(CornerPosition.BottomLeft)}
                 tooltip={ToolTipTexts.freeBoth}
            ></div>
            <div onMouseDown={handleResize(CornerPosition.BottomRight)}
                 style={resizeHandleStyles(CornerPosition.BottomRight)}
                 tooltip={ToolTipTexts.freeBoth}
            ></div>
            <div onMouseDown={handleResize(EdgePosition.Right)}
                 style={resizeHandleStyles(EdgePosition.Right)}
                 tooltip={ToolTipTexts.freeSnap}
            ></div>
            <div onMouseDown={handleResize(EdgePosition.Bottom)}
                 style={resizeHandleStyles(EdgePosition.Bottom)}
                 tooltip={ToolTipTexts.freeSnap}
            ></div>
            <div onMouseDown={handleResize(EdgePosition.Left)}
                 style={resizeHandleStyles(EdgePosition.Left)}
                 tooltip={ToolTipTexts.freeSnap}
            ></div>
            <div onMouseDown={handleResize(EdgePosition.Top)}
                 style={resizeHandleStyles(EdgePosition.Top)}
                 tooltip={ToolTipTexts.freeSnap}
            ></div>
        </div>
    );
};

// Helper function to define styles for different handles
const resizeHandleStyles = (position: ResizeHandlePosition): React.CSSProperties => {
    const baseStyle: React.CSSProperties = {
        position: 'absolute',
        backgroundColor: 'transparent',
    };
    const cornerSize = '10px'; // Size for corner handles
    const edgeThickness = '6px'; // Thickness for edge handles, could be less than cornerSize for visual distinction
    // Define styles with a comprehensive index type
    const styles: { [key: string]: React.CSSProperties } = {
        [CornerPosition.TopLeft]: {left: 0, top: 0, width: cornerSize, height: cornerSize, cursor: 'nwse-resize'},
        [CornerPosition.TopRight]: {right: 0, top: 0, width: cornerSize, height: cornerSize, cursor: 'nesw-resize'},
        [CornerPosition.BottomLeft]: {left: 0, bottom: 0, width: cornerSize, height: cornerSize, cursor: 'nesw-resize'},
        [CornerPosition.BottomRight]: {
            right: 0,
            bottom: 0,
            width: cornerSize,
            height: cornerSize,
            cursor: 'nwse-resize'
        },
        [EdgePosition.Right]: {
            right: 0,
            top: cornerSize,
            bottom: cornerSize,
            width: edgeThickness,
            cursor: 'ew-resize'
        },
        [EdgePosition.Bottom]: {
            bottom: 0,
            left: cornerSize,
            right: cornerSize,
            height: edgeThickness,
            cursor: 'ns-resize'
        },
        [EdgePosition.Left]: {left: 0, top: cornerSize, bottom: cornerSize, width: edgeThickness, cursor: 'ew-resize'},
        [EdgePosition.Top]: {top: 0, left: cornerSize, right: cornerSize, height: edgeThickness, cursor: 'ns-resize'},
    };

    return {...baseStyle, ...styles[position]};
};

export default ResizableDiv;
