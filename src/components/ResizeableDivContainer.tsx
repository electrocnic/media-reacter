import React, {useCallback, useEffect, useRef, useState} from 'react';
import {signal, computed} from '@preact/signals-react';
import type { Signal, ReadonlySignal } from '@preact/signals-react';
import ResizableDiv from "@/components/ResizeableDiv";

import styles from './ResizeableDivContainer.module.css';
import { useLoadDivsConfig } from "@/utils/loadDivConfig.ts";
import {DivConfigDTO} from "@/types/types.ts";

// const generateRandomColor = (): string => '#' + Math.floor(Math.random() * 16777215).toString(16);

const SNAP_DISTANCE = 6;
const UNSNAP_DISTANCE = 70;

const verticalSnaps: Signal<number[]> = signal<(number[])>([]);
const horizontalSnaps: Signal<number[]> = signal<(number[])>([]);
const snapLinesRerenderTrigger = signal<number>(0);

// Parent component that manages multiple ResizableDiv components
const ResizableDivContainer: React.FC = () => {
    const [filePath, setFilePath] = useState('/data/divs_test_00.json');
    const [div, edges] = useLoadDivsConfig(filePath);
    const fileInputRef = useRef<HTMLInputElement>(null);

    // Utility variables for this component
    const initialVerticalSnap = useRef<number | null>(null);
    const initialHorizontalSnap = useRef<number | null>(null);
    const currentMouseX = useRef<number>(0);
    const currentMouseY = useRef<number>(0);
    const throttleAdjustZRef = useRef<boolean>(false);
    const throttleUnsnapRef = useRef<boolean[]>([]);

    const adjustZIndex = useCallback((id: number, direction: 'up' | 'down') => {
        if (throttleAdjustZRef.current) return; // Check if currently throttled
        throttleAdjustZRef.current = true; // Set throttled state
        setTimeout(() => {
            throttleAdjustZRef.current = false; // Reset throttled state after delay
        }, 100); // Throttle period in milliseconds

        const swapZ = div.zIndices.find(otherZIndex => otherZIndex.value === div.zIndices[id].value + (direction === 'up' ? 1 : -1));
        if (!swapZ) return; // No div to swap with

        // Perform the swap
        const temp = div.zIndices[id].value;
        div.zIndices[id].value = swapZ.value;
        swapZ.value = temp;
    }, [div]);

    const compareAndSnapEdges = (
        draggedLeftEdge: ReadonlySignal<number>,
        targetEdges: (readonly [ReadonlySignal<number>, ReadonlySignal<number>])[],
        snaps: Signal<number[]>
    ) => {
        // Initialize an array to hold the closest edge for each dragged edge
        const closestEdges: number[] = [];

        let minDistance = SNAP_DISTANCE + 1; // Reset for each draggedEdge
        let resultCoordinate: number | null = null;
        const resultSnap: (number | null)[] = [null, null];

        for (const targetEdge of targetEdges) {
            const distance = Math.abs(draggedLeftEdge.value - targetEdge[0].value); // Compare with the artifical coordinates
            if (distance <= minDistance) {
                // Found a closer edge within SNAP_DISTANCE for this draggedEdge
                minDistance = distance;
                resultCoordinate = targetEdge[0].value; // Use the left edge coordinate for placement
                resultSnap.pop();
                resultSnap.unshift(targetEdge[1].value); // Use the original edge coordinate for the snap line
            }
        }

        // If a closest edge was found for this draggedEdge, add it to the list
        if (resultCoordinate !== null) {
            closestEdges.push(resultCoordinate);
            for (let i = 0; i < 2; i++) {
                if (resultSnap.length > 0) {
                    const popped = resultSnap.pop();
                    if (popped !== null && popped !== undefined && !snaps.value.includes(popped))
                        snaps.value.push(popped); // It's safe to use ! after confirming it's a number
                        snapLinesRerenderTrigger.value++;
                }
            }
        }

        return closestEdges; // Return a list of closest edges, one per draggedEdge
    };

    const compareAndSnapEdgesRes = (
        resizedEdge: ReadonlySignal<number>[], // 0 = left edge, 1 = right edge (or top bottom)
        targetEdges: (readonly [ReadonlySignal<number>, ReadonlySignal<number>])[],
        leftRightBottomTop: 0|1|-1,
        snaps: Signal<number[]>
    ) => {
        snaps.value = [];

        // Initialize an array to hold the closest edge for each dragged edge
        const closestEdges: number[] = [];
        const closestOpposites: number[] = [];

        let minDistance_l = SNAP_DISTANCE + 1; // Reset for each draggedEdge
        let minDistance_r = SNAP_DISTANCE + 1; // Reset for each draggedEdge
        let resultCoordinate: number | null = null;
        let resultDiameter: number | null = null;
        let resultSnap_l: number | null = null;
        let resultSnap_r: number | null = null;

        for (const targetEdge of targetEdges) {
            const distance_l = Math.abs(resizedEdge[0].value - targetEdge[1].value); // Compare left edge with the original coordinates
            if (distance_l <= minDistance_l) {
                // Found a closer edge within SNAP_DISTANCE for this draggedEdge
                minDistance_l = distance_l;
                if (leftRightBottomTop === -1) { // only set x or y coordinate of resized component if we drag left or top edge
                    resultCoordinate = targetEdge[1].value;
                    resultDiameter = resizedEdge[1].value - targetEdge[1].value; // r - t_l
                }
                resultSnap_l = targetEdge[1].value; // Use the original edge coordinate for the snap line
            }
            const distance_r = Math.abs(resizedEdge[1].value - targetEdge[1].value); // Compare right with the original coordinates
            if (distance_r <= minDistance_r) {
                // Found a closer edge within SNAP_DISTANCE for this draggedEdge
                minDistance_r = distance_r;
                if (leftRightBottomTop === 1) {
                    resultDiameter = targetEdge[1].value - resizedEdge[0].value;
                }
                resultSnap_r = targetEdge[1].value; // Use the original edge coordinate for the snap line
            }
        }

        // If a closest edge was found for this draggedEdge, add it to the list
        if (resultDiameter !== null) {
            closestOpposites.push(resultDiameter);
        }
        if (resultCoordinate !== null) {
            closestEdges.push(resultCoordinate);
        }
        if (resultSnap_l && !snaps.value.includes(resultSnap_l)) {
            snaps.value.push(resultSnap_l); // It's safe to use ! after confirming it's a number
            snapLinesRerenderTrigger.value++;
        }
        if (resultSnap_r && !snaps.value.includes(resultSnap_r)) {
            snaps.value.push(resultSnap_r); // It's safe to use ! after confirming it's a number
            snapLinesRerenderTrigger.value++;
        }

        return [closestEdges, closestOpposites]; // Return a list of closest edges, one per draggedEdge
    };

    const onResize = useCallback((e: MouseEvent, newX: number, newY: number, newWidth: number, newHeight: number, leftRight: 0|1|-1, bottomTop: 0|1|-1, id: number, draggedHorizontalEdges: ReadonlySignal<number>[], draggedVerticalEdges: ReadonlySignal<number>[]) => {
        currentMouseX.current = e.clientX;
        currentMouseY.current = e.clientY;

        // Initial position adjustment without snapping
        let finalX = newX;
        let finalY = newY;
        let finalWidth = newWidth;
        let finalHeight = newHeight;

        // Apply the adjusted position with snapping already once before trying to snap
        div.Xs[id].value = finalX;
        div.Ys[id].value = finalY;
        div.widths[id].value = finalWidth;
        div.heights[id].value = finalHeight;


        if(!e.ctrlKey && !throttleUnsnapRef.current[id]) {
            const horizontalEdges = edges.horizontalEdges[id]; // all except current divId, for all divs
            const verticalEdges = edges.verticalEdges[id];
            const [closestYs, closestHeights] = compareAndSnapEdgesRes(draggedHorizontalEdges, horizontalEdges, bottomTop, horizontalSnaps);
            const [closestXs, closestWidths] = compareAndSnapEdgesRes(draggedVerticalEdges, verticalEdges, leftRight, verticalSnaps);

            // Found snaps if not empty:
            if (closestYs.length || closestXs.length || closestHeights.length || closestWidths.length) {
                // Record the initial snap positions only when the snap first occurs
                if (initialVerticalSnap.current === null || initialHorizontalSnap.current === null) {
                    initialVerticalSnap.current = currentMouseX.current;
                    initialHorizontalSnap.current = currentMouseY.current;
                }

                // Actions during all snap-state iterations
                // Calculate the distance moved since snapping
                const distanceMovedX = Math.abs(currentMouseX.current - initialVerticalSnap.current);
                const distanceMovedY = Math.abs(currentMouseY.current - initialHorizontalSnap.current);

                // Determine if the div should unsnap based on distance moved
                const shouldUnsnap = distanceMovedX > UNSNAP_DISTANCE || distanceMovedY > UNSNAP_DISTANCE;

                if (shouldUnsnap) {
                    // Reset initial snap points if unsnapping
                    throttleUnsnapRef.current[id] = true;
                    setTimeout(() => {
                        throttleUnsnapRef.current[id] = false; // Reset throttled state after delay
                    }, 300); // Throttle period in milliseconds
                    initialVerticalSnap.current = null;
                    initialHorizontalSnap.current = null;
                    // horizontalSnaps.value = [];
                    // verticalSnaps.value = [];
                } else {
                    // Apply snapping if the div should not unsnap
                    finalX = closestXs.length > 0 ? Math.min(...closestXs) : finalX;
                    finalY = closestYs.length > 0 ? Math.min(...closestYs) : finalY;
                    finalWidth = closestWidths.length > 0 ? Math.min(...closestWidths) : finalWidth;
                    finalHeight = closestHeights.length > 0 ? Math.min(...closestHeights) : finalHeight;
                }
            }
        }

        // Apply the adjusted position with snapping
        div.Xs[id].value = finalX;
        div.Ys[id].value = finalY;
        div.widths[id].value = Math.max(finalWidth, 20);
        div.heights[id].value = Math.max(finalHeight, 20);
    }, [div.Xs, div.Ys, div.widths, div.heights, currentMouseX.current, currentMouseY.current, edges, initialVerticalSnap, initialHorizontalSnap, horizontalSnaps.value, verticalSnaps.value, snapLinesRerenderTrigger.value]);

    const handleDrag = useCallback((
        e: MouseEvent, currentX: number, currentY: number, id: number, draggedHorizontalEdges: ReadonlySignal<number>[], draggedVerticalEdges: ReadonlySignal<number>[]
    ) => {
        currentMouseX.current = e.clientX;
        currentMouseY.current = e.clientY;

        // Initial position adjustment without snapping
        let finalX = currentX;
        let finalY = currentY;

        if(!e.altKey && !throttleUnsnapRef.current[id]) {
            const horizontalEdges = edges.horizontalEdges[id]; // all except current divId, for all divs
            const verticalEdges = edges.verticalEdges[id];
            const closestHorizontalEdges = compareAndSnapEdges(draggedHorizontalEdges[0], horizontalEdges, horizontalSnaps);
            const closestVerticalEdges = compareAndSnapEdges(draggedVerticalEdges[0], verticalEdges, verticalSnaps);

            // Found snaps if not empty:
            if (closestHorizontalEdges.length || closestVerticalEdges.length) {
                // Record the initial snap positions only when the snap first occurs
                if (initialVerticalSnap.current === null || initialHorizontalSnap.current === null) {
                    initialVerticalSnap.current = currentMouseX.current;
                    initialHorizontalSnap.current = currentMouseY.current;
                }

                // Actions during all snap-state iterations
                // Calculate the distance moved since snapping
                const distanceMovedX = Math.abs(currentMouseX.current - initialVerticalSnap.current);
                const distanceMovedY = Math.abs(currentMouseY.current - initialHorizontalSnap.current);

                // Determine if the div should unsnap based on distance moved
                const shouldUnsnap = distanceMovedX > UNSNAP_DISTANCE || distanceMovedY > UNSNAP_DISTANCE;

                if (shouldUnsnap) {
                    // Reset initial snap points if unsnapping
                    throttleUnsnapRef.current[id] = true;
                    setTimeout(() => {
                        throttleUnsnapRef.current[id] = false; // Reset throttled state after delay
                    }, 300); // Throttle period in milliseconds
                    initialVerticalSnap.current = null;
                    initialHorizontalSnap.current = null;
                    horizontalSnaps.value = [];
                    verticalSnaps.value = [];
                } else {
                    // Apply snapping if the div should not unsnap
                    finalX = closestVerticalEdges.length > 0 ? Math.min(...closestVerticalEdges) : finalX;
                    finalY = closestHorizontalEdges.length > 0 ? Math.min(...closestHorizontalEdges) : finalY;
                }
            }
        }

        // Apply the adjusted position with snapping
        div.Xs[id].value = finalX;
        div.Ys[id].value = finalY;
    }, [div.Xs, div.Ys, edges, initialVerticalSnap, initialHorizontalSnap, horizontalSnaps.value, verticalSnaps.value, snapLinesRerenderTrigger.value]);

    useEffect(() => {
        const handleSaveShortcut = (event: KeyboardEvent) => {
            if (event.ctrlKey && event.key === 's') {
                event.preventDefault(); // Prevent the browser's save dialog
                saveDivsConfigToFile();
            }
        };

        document.addEventListener('keydown', handleSaveShortcut);
        return () => document.removeEventListener('keydown', handleSaveShortcut);
    }, [div, edges]); // Make sure to re-bind the event if div or edges change

    useEffect(() => {
        const handleOpenShortcut = (event: KeyboardEvent) => {
            if (event.ctrlKey && event.key === 'o') {
                event.preventDefault(); // Prevent the default open dialog
                fileInputRef.current?.click(); // Programmatically click the file input to open the file dialog
            }
        };

        document.addEventListener('keydown', handleOpenShortcut);
        return () => document.removeEventListener('keydown', handleOpenShortcut);
    }, []);

    const saveDivsConfigToFile = () => {
        const divConfigs = div.ids.map((_, index): DivConfigDTO => ({
            x: div.Xs[index].value,
            y: div.Ys[index].value,
            width: div.widths[index].value,
            height: div.heights[index].value,
            zIndex: div.zIndices[index].value,
            backgroundColor: div.backgroundColors[index]
        }));

        const blob = new Blob([JSON.stringify(divConfigs, null, 2)], { type: 'application/json' });
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'divs_config.json'; // Default file name, user can change it
        a.click();
        URL.revokeObjectURL(url); // Clean up the URL object
    };

    const handleFileSelected = async (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];
        if (!file) return;

        // Since useLoadDivsConfig accepts a file path, let's assume you have a way to serve the uploaded file.
        // For local files handling, you might need to read the file content here and then manage how to use it with your hook.
        // This example assumes the hook can directly use the file content or a blob URL.

        const blobUrl = URL.createObjectURL(file); // Create a URL for the blob
        setFilePath(blobUrl); // Update the state to re-trigger the hook
    };

    return (
        <div style={{display: 'flex', flexWrap: 'wrap', position: 'relative'}}>
            <input type="file" style={{display: 'none'}} ref={fileInputRef} onChange={handleFileSelected}
                   accept=".json"/>
            {div.Xs.map((_, index) => (
                <ResizableDiv
                    key={index}
                    id={div.ids[index]}
                    x={div.Xs[index]}
                    y={div.Ys[index]}
                    width={div.widths[index]}
                    height={div.heights[index]}
                    horizontalEdges={[div.Ys[index], computed(() => div.Ys[index].value + div.heights[index].value)]}
                    verticalEdges={[div.Xs[index], computed(() => div.Xs[index].value + div.widths[index].value)]}
                    zIndex={div.zIndices[index]}
                    backgroundColor={div.backgroundColors[index]}
                    onDrag={handleDrag}
                    onAdjustZIndex={adjustZIndex}
                    onResize={onResize}
                    horizontalSnaps={horizontalSnaps}
                    verticalSnaps={verticalSnaps}
                />
            ))}
            {verticalSnaps.value.map((snap, idx) => (
                <div key={`snap-v-${idx}`} className={styles.lineX} style={{
                    position: 'absolute',
                    top: 0,
                    bottom: 0,
                    left: snap,
                    height: 'auto',
                    width: '1px',
                    zIndex: 599
                }}></div>
            ))}
            {horizontalSnaps.value.map((snap, idx) => (
                <div key={`snap-h-${idx}`} className={styles.lineY} style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    top: snap,
                    width: 'auto',
                    height: '1px',
                    zIndex: 599
                }}></div>
            ))}
        </div>
    );
};

export default ResizableDivContainer;
