import '@/assets/scss/main.scss'

import App from '@/App'
import ReactDOM from 'react-dom/client'
import { StrictMode } from 'react'

ReactDOM.createRoot(document.getElementById('root')!).render(
  <StrictMode>
    <App />
  </StrictMode>,
)
