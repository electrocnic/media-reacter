import React from 'react';

declare module 'react' {
    interface HTMLAttributes<T> extends React.AriaAttributes, React.DOMAttributes<T> {
        // Extend React's HTMLAttributes for `div` elements to include a `tooltip` prop.
        tooltip?: string;
    }
}
