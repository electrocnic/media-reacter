import type { Signal, ReadonlySignal } from '@preact/signals-react';

export interface SnapHelperlineProps {
    verticalSnaps: Signal<number[]>;
    horizontalSnaps: Signal<number[]>;
}

// Type for the raw data loaded from JSON
export interface DivConfigDTO {
    x: number;
    y: number;
    width: number;
    height: number;
    zIndex: number;
    backgroundColor: string;
}

// Type for the in-memory representation with signals
export interface ReactiveDivsConfig {
    ids: number[];
    Xs: Signal<number>[];
    Ys: Signal<number>[];
    widths: Signal<number>[];
    heights: Signal<number>[];
    zIndices: Signal<number>[];
    backgroundColors: string[];
}

export interface ReactiveEdges {
    leftEdges: ReadonlySignal<number>[];
    rightEdges: ReadonlySignal<number>[];
    topEdges: ReadonlySignal<number>[];
    bottomEdges: ReadonlySignal<number>[];
    horizontalEdges: (readonly [ReadonlySignal<number>, ReadonlySignal<number>])[][]; // input currentDivId in the outer array to get a list of all horizontalEdges relative to that current div
    verticalEdges: (readonly [ReadonlySignal<number>, ReadonlySignal<number>])[][]; // tuple selection: 0 = artifical (or original) edge; 1 = original edge always
}

export interface ReactiveDivProps {
    id: number;
    x: Signal<number>;
    y: Signal<number>;
    width: Signal<number>;
    height: Signal<number>;
    horizontalEdges: ReadonlySignal<number>[];
    verticalEdges: ReadonlySignal<number>[];
    zIndex: Signal<number>;
    backgroundColor: string;
}

export interface DivEventProps {
    onDrag: (e: MouseEvent, x: number, y: number, id: number, draggedHorizontalEdges: ReadonlySignal<number>[], draggedVerticalEdges: ReadonlySignal<number>[]) => void; // Callback to update position
    onAdjustZIndex: (id: number, direction: 'up' | 'down') => void;
    onResize: (e: MouseEvent, newX: number, newY: number, newWidth: number, newHeight: number, leftRight: 0|1|-1, bottomTop: 0|1|-1, id: number, draggedHorizontalEdges: ReadonlySignal<number>[], draggedVerticalEdges: ReadonlySignal<number>[]) => void; // Callback to update position
}

//export type PersistableDivConfig = DivStyleConfig;
export type ResizableDivProps = ReactiveDivProps & DivEventProps & SnapHelperlineProps;

export interface SnapCalculationArgs {
    draggedDiv: ResizableDivProps;
    targetDiv: ResizableDivProps;
    currentX: number;
    currentY: number;
}

export interface SnapCalculationResults {
    finalX: number;
    finalY: number;
    snapXOccurred: boolean;
    snapYOccurred: boolean;
}

export interface SnapOnResizeCalculationArgs {
    resizedDiv: ResizableDivProps;
    targetDiv: ResizableDivProps;
    currentX: number;
    currentY: number;
    currentWidth: number;
    currentHeight: number;
}

export interface SnapOnResizeCalculationResults {
    finalX: number;
    finalY: number;
    finalWidth: number;
    finalHeight: number;
    snapXOccurred: boolean;
    snapYOccurred: boolean;
}

export enum EdgePosition {
    Right = 'right',
    Bottom = 'bottom',
    Left = 'left',
    Top = 'top'
}
export enum CornerPosition {
    TopLeft = 'top-left',
    TopRight = 'top-right',
    BottomLeft = 'bottom-left',
    BottomRight = 'bottom-right'
}

export type ResizeHandlePosition = EdgePosition | CornerPosition;

export function isInEnum(value: string, enumeration: object): boolean {
    return Object.values(enumeration).includes(value);
}

