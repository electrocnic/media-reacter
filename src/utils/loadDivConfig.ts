import {useEffect, useState} from 'react';
import { signal, computed } from "@preact/signals-react";
import type { ReadonlySignal, Signal } from "@preact/signals-react";
import type { ReactiveDivsConfig, DivConfigDTO, ReactiveEdges } from "@/types/types.ts";

export const useLoadDivsConfig = (configFilePath: string): [ReactiveDivsConfig, ReactiveEdges] => {
    const [divConfigs, setDivConfigs] = useState<ReactiveDivsConfig>({
        ids: [],
        Xs: [],
        Ys: [],
        widths: [],
        heights: [],
        zIndices: [],
        backgroundColors: [],
    });

    const [edges, setEdges] = useState<ReactiveEdges>({
        leftEdges: [],
        rightEdges: [],
        topEdges: [],
        bottomEdges: [],
        horizontalEdges: [],
        verticalEdges: [],
    });

    useEffect(() => {
        const loadConfig = async () => {
            const response = await fetch(configFilePath);
            const rawConfigs: DivConfigDTO[] = await response.json();

            const loadedDivs: ReactiveDivsConfig = {
                ids: rawConfigs.map((_, index) => index),
                Xs: rawConfigs.map(config => signal(config.x)),
                Ys: rawConfigs.map(config => signal(config.y)),
                widths: rawConfigs.map(config => signal(config.width)),
                heights: rawConfigs.map(config => signal(config.height)),
                zIndices: rawConfigs.map(config => signal(config.zIndex)),
                backgroundColors: rawConfigs.map(config => config.backgroundColor),
            };

            const leftEdges = loadedDivs.Xs.map(x => x as ReadonlySignal<number>);
            const rightEdges = loadedDivs.Xs.map((x, i) => computed(() => x.value + loadedDivs.widths[i].value));
            const topEdges = loadedDivs.Ys.map(y => y as ReadonlySignal<number>);
            const bottomEdges = loadedDivs.Ys.map((y, i) => computed(() => y.value + loadedDivs.heights[i].value));

            const computeEdges = (
                edges1: Signal<number>[],  // Edges from which to subtract (e.g., rightEdges or bottomEdges)
                edges2: Signal<number>[],  // Edges to keep as is (e.g., leftEdges or topEdges)
                dimension: Signal<number>, // The dimension to subtract (e.g., width or height)
                excludeDivId: number       // The divId to exclude
            ) => {
                // Keep edges 1 once as is
                const computedOriginalEdges1 = edges1
                    .filter((_, divId) => divId !== excludeDivId) // Exclude the current div's edges
                    .map(edge => [edge, edge] as const);
                // Add artificial shifted edges for detection and snap of opposite edge
                const computedArtificialEdges1 = edges1
                    .filter((_, divId) => divId !== excludeDivId) // Exclude the current div's edges
                    .map(edge => [computed(() => edge.value - dimension.value), edge] as const);
                // Filter and keep edges2 as is, but still in tuple form for consistency
                const computedOriginalEdges2 = edges2
                    .filter((_, divId) => divId !== excludeDivId) // Exclude the current div's edges
                    .map(edge => [edge, edge] as const);
                // Add artificial shifted edges for detection and snap of opposite edge
                const computedArtificialEdges2 = edges2
                    .filter((_, divId) => divId !== excludeDivId) // Exclude the current div's edges
                    .map(edge => [computed(() => edge.value - dimension.value), edge] as const);
                // Combine the two lists of tuples
                return [...computedOriginalEdges1, ...computedArtificialEdges1, ...computedOriginalEdges2, ...computedArtificialEdges2];
            };
            const horizontalEdges: (readonly [ReadonlySignal<number>, ReadonlySignal<number>])[][] = loadedDivs.heights.map(
                    (height, index) => computeEdges(bottomEdges, topEdges, height, index)
            );
            const verticalEdges: (readonly [ReadonlySignal<number>, ReadonlySignal<number>])[][] = loadedDivs.widths.map(
                (width, index) => computeEdges(rightEdges, leftEdges, width, index)
            );
            const loadedEdges: ReactiveEdges = { leftEdges, rightEdges, topEdges, bottomEdges, horizontalEdges, verticalEdges };

            setDivConfigs(loadedDivs);
            setEdges(loadedEdges);
        };

        loadConfig();
    }, [configFilePath]);

    return [divConfigs, edges];
};
