// useTooltipInjector.ts
import { useEffect } from 'react';

const createTooltipElement = (text: string) => {
    const tooltipElement = document.createElement('div');
    tooltipElement.textContent = text;
    tooltipElement.style.position = 'absolute';
    tooltipElement.style.zIndex = '1000';
    tooltipElement.style.background = 'black';
    tooltipElement.style.color = 'white';
    tooltipElement.style.padding = '4px 8px';
    tooltipElement.style.borderRadius = '4px';
    tooltipElement.style.whiteSpace = 'nowrap';
    tooltipElement.style.visibility = 'hidden'; // initially hidden
    document.body.appendChild(tooltipElement);
    return tooltipElement;
};

const useTooltipInjector = () => {
    useEffect(() => {
        const handleMouseOver = (e: MouseEvent) => {
            const target = e.target as HTMLElement;
            const tooltipText = target.getAttribute('tooltip');
            if (tooltipText) {
                const tooltipElement = createTooltipElement(tooltipText);
                const { left, top } = target.getBoundingClientRect();
                tooltipElement.style.left = `${left}px`;
                tooltipElement.style.top = `${top - tooltipElement.offsetHeight - 5}px`;
                tooltipElement.style.visibility = 'visible';

                const handleMouseOut = () => {
                    tooltipElement.remove();
                    target.removeEventListener('mouseout', handleMouseOut);
                };

                target.addEventListener('mouseout', handleMouseOut);
            }
        };

        document.addEventListener('mouseover', handleMouseOver);

        return () => {
            document.removeEventListener('mouseover', handleMouseOver);
        };
    }, []);
};

export default useTooltipInjector;
