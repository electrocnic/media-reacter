import { render, screen } from '@testing-library/react'
import ResizeableDiv from '@/components/ResizeableDiv'

vi.mock('@microsoft/mgt-react', () => {
  return {
    Person: () => ({}),
    Providers: {},
  }
})

describe('ResizeableDiv', () => {
  it('snaps', () => {
    render(<ResizeableDiv />)
    expect(screen.getByText('Aktive Baustellen')).toBeInTheDocument()
  })
})
