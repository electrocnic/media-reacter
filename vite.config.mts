/// <reference types="vitest" />
import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'


// https://vitejs.dev/config/
export default defineConfig({
    plugins: [react({
        // Integrate Babel plugins here
        babel: {
            plugins: [['module:@preact/signals-react-transform']],
        },
    })],
    resolve: {
        alias: [
            // Alias for general '@' references to point to the 'src' directory
            { find: '@', replacement: fileURLToPath(new URL('./src', import.meta.url)) },

            // Specific alias for '@/components' to point to the literal '@/components' directory at the project root
            // This assumes there is a directory named '@' at your project root, containing a 'components' directory
            { find: 'shadcn', replacement: fileURLToPath(new URL('./shadcn', import.meta.url)) },
        ]
    },
    test: {
        include: ['./tests/**/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}'],
        setupFiles: ['./tests/vitest.setup.ts'],
        globals: true,
        environment: 'happy-dom',
        typecheck: {
            tsconfig: './tsconfig.vitest.json',
            enabled: true,
        },
        coverage: {
            include: ['src/**'],
        },
    },
})
